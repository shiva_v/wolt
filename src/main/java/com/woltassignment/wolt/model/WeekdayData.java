package com.woltassignment.wolt.model;

import lombok.Data;

@Data
public class WeekdayData {
    private String type;
    private Integer value;
}
