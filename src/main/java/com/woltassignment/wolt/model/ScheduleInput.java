package com.woltassignment.wolt.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ScheduleInput {
    private Map<String, List<WeekdayData>> listOfWeekdays;
}
