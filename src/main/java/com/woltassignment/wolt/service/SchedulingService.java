package com.woltassignment.wolt.service;

import com.woltassignment.wolt.model.WeekdayData;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class SchedulingService {
    public Map<String, String> reformatRestaurantTimetable(Map<String, List<WeekdayData>> listOfWeekdays) {
        Map<String, String> weekSchedule = new HashMap<>();
        String formattedTime = "";
        String previousDay = "";
        Boolean countAsSameDay = false;
        for (String day : listOfWeekdays.keySet()) {
            List<WeekdayData> listOfHourData = listOfWeekdays.get(day);
            formattedTime = listOfHourData.size() > 0 ? "" : "Closed";
            if (listOfHourData != null && listOfHourData.size() > 0) {
                Iterator<WeekdayData> iterator = listOfHourData.iterator();
                while (iterator.hasNext()) {
                    WeekdayData weekdayData = iterator.next();
                    if (weekdayData.getType().equalsIgnoreCase("open")) {
                        formattedTime = convertTo12HourFormat(weekdayData.getValue());
                    } else {
                        formattedTime = " - " + convertTo12HourFormat(weekdayData.getValue());
                    }
                    formattedTime = listOfHourData.size() > 2 && listOfHourData.size() % 2 == 0 ? formattedTime.concat(", ") : formattedTime;
                    if (!iterator.hasNext()) {
                        if (weekdayData.getType().equalsIgnoreCase("open")) {
                            countAsSameDay = true;
                            previousDay = day;
                        }
                    }
                    if (countAsSameDay && previousDay.length() > 0) {
                        String currentFormattedTime = weekSchedule.get(previousDay) != null && weekSchedule.get(previousDay).length() > 0 ? weekSchedule.get(previousDay) : "";
                        weekSchedule.put(previousDay, currentFormattedTime.concat(formattedTime));
                        if (weekdayData.getType().equalsIgnoreCase("close")) {
                            previousDay = "";
                            countAsSameDay = false;
                            formattedTime = "";
                        }
                    } else {
                        String currentFormattedTime = weekSchedule.get(day) != null && weekSchedule.get(day).length() > 0 ? weekSchedule.get(day) : "";
                        weekSchedule.put(day, currentFormattedTime.concat(formattedTime));
                    }
                }
            } else {
                weekSchedule.put(day, formattedTime);
            }
        }
        return weekSchedule;
    }

    private String convertTo12HourFormat(int timeInSeconds) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        df.setTimeZone(tz);
        String time = df.format(new Date(timeInSeconds * 1000L));
        return time;
    }
}
