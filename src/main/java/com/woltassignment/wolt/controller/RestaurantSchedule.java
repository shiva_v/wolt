package com.woltassignment.wolt.controller;

import com.woltassignment.wolt.model.WeekdayData;
import com.woltassignment.wolt.service.SchedulingService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class RestaurantSchedule {

    @PostMapping("/restaurant-schedule")
    Map<String, String> fetchSchedule(@RequestBody Map<String, List<WeekdayData>> listOfWeekdays) {
        SchedulingService schedulingService = new SchedulingService();
        Map<String, String> weekSchedule = schedulingService.reformatRestaurantTimetable(listOfWeekdays);
        return weekSchedule;
    }
}
