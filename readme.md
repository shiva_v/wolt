# Restaurant scheduling service
This application accepts opening hours data as input in JSON and returns a more  
human readable version of the data formatted using  a 12-hour clock.
The problem statement is detailed in this [document](https://c.smartrecruiters.com/sr-company-attachments-prod-dc5/5f05b5736bbcbc0ff2f7e7f3/b8fb6fdc-ce69-433a-8774-9f8244d74efa?r=s3-eu-central-1).

# Steps to setup on local

Unzip the attached zip file and run this command in the directory to install dependencies through gradle & build a JAR.

    ./gradlew build  

After running this, spin up the server by running the main class of **WoltApplication.java**.

## Testing the API on local

To test the API on local, please hit this curl with the payload of your choice conforming to the format specified in the coding assignment [document](https://c.smartrecruiters.com/sr-company-attachments-prod-dc5/5f05b5736bbcbc0ff2f7e7f3/b8fb6fdc-ce69-433a-8774-9f8244d74efa?r=s3-eu-central-1)


> curl --location --request POST 'localhost:8080/restaurant-schedule' \
--header 'Content-Type: application/json' \
--data-raw '{
"monday": [],
"tuesday": [
{
"type": "open",
"value": 36000
},
{
"type": "close",
"value": 64800
}
],
"wednesday": [],
"thursday": [
{
"type": "open",
"value": 37800
},
{
"type": "close",
"value": 64800
}
],
"friday": [
{
"type": "open",
"value": 36000
}
],
"saturday": [
{
"type": "close",
"value": 3600
},
{
"type": "open",
"value": 36000
}
],
"sunday": [
{
"type": "close",
"value": 3600
},
{
"type": "open",
"value": 43200
},
{
"type": "close",
"value": 75600
}
]
}'

## Alternate approach to accept data in a different format

The current format as specified in the document is alright for production use but I would change the JSON structure to something like

> {
"monday": [],
"tuesday": [
{
"open": 36000,
"close": 64800
}
],
"wednesday": [],
"thursday": [
{
"open": 37800,
"close": 64800
}
],
"friday": [
{
"open": 36000
}
],
"saturday": [
{
"close": 3600
},
{
"open": 36000
}
],
"sunday": [
{
"close": 3600
},
{
"open": 43200,
"close": 75600
}
]
}

This structure is easier to parse on the backend since all the pairs are tagged together and the carry over logic of opening the restaraunt one day and closing it on the next day becomes simpler to understand and implement.

